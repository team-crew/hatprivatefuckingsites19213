# -*- coding: utf-8 -*-

'''
    jizzplanet Add-on

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    Adapted from the alfa-addon code
'''

import inspect

import xbmc, xbmcaddon

import sys, six
PY3 = sys.version_info[0] >= 3
unicode = six.text_type
loggeractive = True


def log_enable(active):
    global loggeractive
    loggeractive = active


def encode_log(message=""):
    
    # Unicode to utf8
    if isinstance(message, unicode):
        message = message.encode("utf8")
        if PY3: message = message.decode("utf8")

    # All encodings to utf8
    elif not PY3 and isinstance(message, str):
        message = unicode(message, "utf8", errors="replace").encode("utf8")
    
    # Bytes encodings to utf8
    elif PY3 and isinstance(message, bytes):
        message = message.decode("utf8")

    # Objects to string
    else:
        message = str(message)

    return message


def get_caller(message=None):
    
    if message and isinstance(message, unicode):
        message = message.encode("utf8")
        if PY3: message = message.decode("utf8")
    elif message and PY3 and isinstance(message, bytes):
        message = message.decode("utf8")
    elif message and not PY3:
        message = unicode(message, "utf8", errors="replace").encode("utf8")
    elif message:
        message = str(message) 
    
    module = inspect.getmodule(inspect.currentframe().f_back.f_back)

    if module == None:
        module = "None"
    else:
        module = module.__name__

    function = inspect.currentframe().f_back.f_back.f_code.co_name

    if module == "__main__":
        module = xbmcaddon.Addon().getAddonInfo('name')
    else:
        module = xbmcaddon.Addon().getAddonInfo('name') + "." + module
    if message:
        if module not in message:
            if function == "<module>":
                return module + " " + message
            else:
                return module + " [" + function + "] " + message
        else:
            return message
    else:
        if function == "<module>":
            return module
        else:
            return module + "." + function


def info(texto="", force=False):
    if loggeractive or force:
        if PY3:
            xbmc.log(get_caller(encode_log(texto)), xbmc.LOGINFO)
        else:
            xbmc.log(get_caller(encode_log(texto)), xbmc.LOGNOTICE)


def debug(texto="", force=False):
    if loggeractive or force:
        texto = "    [" + get_caller() + "] " + encode_log(texto)

        if PY3:
            xbmc.log("######## DEBUG #########", xbmc.LOGINFO)
            xbmc.log(texto, xbmc.LOGINFO)
        else:
            xbmc.log("######## DEBUG #########", xbmc.LOGNOTICE)
            xbmc.log(texto, xbmc.LOGNOTICE)


def error(texto="", force=False):
    texto = "    [" + get_caller() + "] " + encode_log(texto)

    xbmc.log("######## ERROR #########", xbmc.LOGERROR)
    xbmc.log(texto, xbmc.LOGERROR)


class WebErrorException(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)
